<?php

namespace App\Block;



use Doctrine\ORM\EntityManagerInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\Service\BlockServiceInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;

class DashblockService extends AbstractBlockService implements BlockServiceInterface
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;

    }



    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([

            'title' => 'title',
            'template' => 'sonata/admin/dashblock.html.twig',
            'extraVariable' => "extraVariable",
        ]);
    }


    public function execute(BlockContextInterface $blockContext, ?Response $response = null): Response
    {
        $settings = $blockContext->getSettings();

        $content = $this->twig->render($blockContext->getTemplate(), [
            'title' => $settings['title'],
            'extraVariable' => $settings['extraVariable'],
            'settings' => $settings,
        ]);

        if (null === $response) {
            $response = new Response();
        }

        $response->setContent($content);

        return $response;
    }

}
