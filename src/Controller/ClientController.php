<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Repository\MessageRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use GuzzleHttp\Client;
class ClientController extends AbstractController
{
    #[Route('/client', name: 'app_client', methods:"GET")]
    public function index(Request $request,MessageRepository $messageRepositorys): Response
    {
        $name = $request->query->get('name');
        $phone = $request->query->get('phone');
        $email = $request->query->get('email');
        $userData =new Customer();
        $userData->setEmail($email);
        $userData->setPhone($phone);
        $userData->setName($name);
        $userData->setCreatedAt(new DateTimeImmutable());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($userData);
        $entityManager->flush();
        $messageRp = $messageRepositorys->findOneBy(['status'=> 1]);
        $message = $messageRp->getMessage();
        $this->sendSMS($phone, $message,$name);

        return new Response('Data saved successfully');
    }


    public function sendSMS($phone,$message,$name): Response
    {
        $message = str_replace('[name]', $name, $message);
        $client = new Client();

        // Paramètres de la requête
        $apiUrl = 'https://api.lexico-sms.com/api/SendSMS';
        $body = [
            "api_id" => "API97264389648",
            "api_password" => "4hFrFfAecd",
            "sms_type" => "P",
            "encoding" => "T",
            "sender_id" => "ASMSC",
            "phonenumber" => $phone,
            "templateid" => null,
            "textmessage" => $message, // Utilisation de $message modifié
            "V1" => null,
            "V2" => null,
            "V3" => null,
            "V4" => null,
            "V5" => null,
            "ValidityPeriodInSeconds" => 60,
            "uid" => "xyz",
            "callback_url" => "https://xyz.com/",
            "pe_id" => "NULL",
            "template_id" => "NULL"
        ];

        // Effectuer la requête POST
        $response = $client->post($apiUrl, [
            'json' => $body
        ]);
        $responseBody = $response->getBody()->getContents();

        // Convertir le JSON en tableau associatif
        $responseData = json_decode($responseBody, true);

        // Récupérer la variable "remarks" de la réponse
        $status = $responseData['status'] ?? null;

        // Retourner la réponse avec le statut
        return new Response('Status: ' . $status);
    }
    #[Route('/client/v1/', name: 'webhook1', methods:"GET")]
    public function webhook(Request $request,MessageRepository $messageRepositorys): Response
    {
        $name = $request->query->get('name');
        $phone = $request->query->get('phone');
        $email = $request->query->get('email');
        $message = $request->query->get('message_id');
        $userData =new Customer();
        $userData->setEmail($email);
        $userData->setPhone($phone);
        $userData->setName($name);
        $userData->setCreatedAt(new DateTimeImmutable());

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($userData);
        $entityManager->flush();
        $messageRp = $messageRepositorys->findOneBy(['id'=> $message]);
        if (!$messageRp){
            return new Response('Message id not found');
        }
        $message = $messageRp->getMessage();
        $this->sendSMS($phone, $message,$name);

        return new Response('Data saved successfully');
    }




    public function home(){
        return   $this->redirect("/admin");
    }
}
