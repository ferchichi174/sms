<?php


namespace App\Admin;


use App\Entity\DIST;
use App\Entity\LOCALITE;
use App\Entity\ROLE;
use App\Entity\TSP;
use App\Entity\User;
use App\Repository\TSPRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollectionInterface;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Admin\Entity\UserAdmin as BaseUserAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\UserBundle\Entity\UserManager;
use Sonata\UserBundle\Form\Type\RolesMatrixType;
use Sonata\UserBundle\Model\UserManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class UserAdmin extends AbstractAdmin
{


    protected function configureRoutes(RouteCollectionInterface $collection): void
    {
        parent::configureRoutes($collection);

        // Supprimer l'action d'export
        $collection->remove('export');
    }





    protected function configureHeader(Request $request): string
    {
        // Personnalisez le titre ici
        return 'Gestion des Utilisateurs';
    }


    protected function configureFormFields(FormMapper $formMapper): void
    {
        $entity = $this->getSubject();

        // Ajouter/supprimer/modifier les champs du formulaire d'édition
        $formMapper
            ->with('general', ['class' => 'col-md-12'])
            ->add('username', null, ["label" => "Utilisateurs"])
            ->add('email')
            ->add('plainPassword', PasswordType::class, [
                'required' => (!$this->hasSubject() || null === $this->getSubject()->getId()),
            ]);






        $formMapper
            ->add('enabled', null);



        ;


        // Ajoutez d'autres champs personnalisés ici

    }


    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->addIdentifier('username', null, ["label" => "Utilisateurs"])
            ->add('enabled', null, [
                'editable' => true, // Allow editing directly from the list view
                'label' => "Etat"
            ])
            ->add('role', EntityType::class, [
                'label' => 'Profile',
                'class' => ROLE::class,
                'choice_label' => 'ROLE_NAME',
                'required' => false,
            ])
            ->add('_action', 'actions', [
                'label' => 'Actions',
                'actions' => [
                    'show' => [
                        'template' => 'sonata/Crud/list_action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'sonata/Crud/list_action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'sonata/Crud/list_action_delete.html.twig',
                    ],
                ],
            ]);
    }


    protected function configureShowFields(ShowMapper $show): void
    {

        $show->add('id', null, [
            'label' => 'id']);
        $show->add('username', null, ["label" => "Utilisateurs"]);
        $show->add('email');
        $show->add('role', null, [
            'label' => 'Profil']);

    }





}
