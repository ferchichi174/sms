<?php
namespace App\Admin;

use App\Entity\Customer;
use phpDocumentor\Reflection\Types\Boolean;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\BooleanType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class CustomerAdmin extends AbstractAdmin
{
    public function toString(object $object): string
    {
        return $object instanceof Customer
            ? 'Customer'
            : 'Customer'; // shown in the breadcrumb on the create view
    }



    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('email', TextType::class);
        $form->add('name', TextType::class);
        $form->add('phone', TextType::class);
        $form->add('status', BooleanType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid->add('email');
        $datagrid->add('name');
        $datagrid->add('phone');
        $datagrid->add('status');
        $datagrid->add('created_at');

    }

    protected function configureListFields(ListMapper $list): void
    {
        $list->addIdentifier('email');
        $list->addIdentifier('name');
        $list->addIdentifier('Phone',null,['label' => 'Phone number']);
        $list->addIdentifier('status',null,['label' => 'Status']);
        $list->addIdentifier('created_at');

        $list->add(ListMapper::NAME_ACTIONS, null, [
            'actions' => [
                'show' => [
                    'template' => 'sonata/Crud/list_action_show.html.twig',
                ],
                'edit' => [
                    'template' => 'sonata/Crud/list_action_edit.html.twig',
                ],
                'delete' => [
                    'template' => 'sonata/Crud/list_action_delete.html.twig',
                ],
            ]
        ]);
    }

    protected function configureShowFields(ShowMapper $show): void
    {
        $show->add('email');
        $show->add('name');
        $show->add('phone');
        $show->add('created_at');

    }

}
